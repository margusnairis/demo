package ee.middleware.demo.springboot;

import ee.middleware.demo.springboot.person.Person;
import ee.middleware.demo.springboot.person.PersonController;
import ee.middleware.demo.springboot.person.PersonService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PersonControllerTests {

    @MockBean
    PersonService personService;

    @Autowired
    PersonController personController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void findPersonByName() throws Exception {
        // given
        Person person = getNewPerson();

        // when
        Mockito.when(personService.findByName(person.getName()))
                .thenReturn(List.of(person));

        // do
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/person?name=" + person.getName())
                .accept(MediaType.APPLICATION_JSON);

        MockHttpServletResponse response = mockMvc
                .perform(requestBuilder)
                .andReturn()
                .getResponse();

        // then
        assertEquals(response.getStatus(), HttpStatus.OK.value());
    }

    private Person getNewPerson() {
        return Person.builder()
                .id(1L)
                .name("Robin")
                .description("developer")
                .createdAt(new Date())
                .createdBy("test")
                .build();
    }

}
