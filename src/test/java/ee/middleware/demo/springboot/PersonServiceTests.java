package ee.middleware.demo.springboot;

import ee.middleware.demo.springboot.person.Person;
import ee.middleware.demo.springboot.person.PersonRepository;
import ee.middleware.demo.springboot.person.PersonService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PersonServiceTests {

    @MockBean
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @Test
    public void findPersonByName() {
        // given
        Person person = getNewPerson();

        // when
        Mockito.when(personRepository.findByName(person.getName()))
                .thenReturn(List.of(person));

        // do
        Person personFromDb = personService.findByName(person.getName()).get(0);

        // then
        verify(personRepository, times(1)).findByName(person.getName());
        assertThat(personFromDb.getName()).isEqualTo(person.getName());
    }

    private Person getNewPerson() {
        return Person.builder()
                .id(1L)
                .name("Robin")
                .description("developer")
                .createdAt(new Date())
                .createdBy("test")
                .build();
    }

}
