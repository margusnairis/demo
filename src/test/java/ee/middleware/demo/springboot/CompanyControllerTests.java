package ee.middleware.demo.springboot;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.middleware.demo.springboot.company.CompanyController;
import ee.middleware.demo.springboot.company.dto.CompanyDto;
import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.company.service.CompanyService;
import ee.middleware.demo.springboot.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class CompanyControllerTests {

    @MockBean
    CompanyService companyService;

    @Autowired
    CompanyController companyController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void findCompanyByName() throws Exception {
        // given
        Company company = getNewCompany();

        // when
        Mockito.when(companyService.findByName(company.getName()))
                .thenReturn(company);

        // do
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/company?name=" + company.getName())
                .accept(MediaType.APPLICATION_JSON);

        MockHttpServletResponse response = mockMvc
                .perform(requestBuilder)
                .andReturn()
                .getResponse();

        // System.out.println("RESPONSE:" + response.getContentAsString());

        // NB! @AllArgsConstructor is not enough and requires also @RequiredArgsConstructor annotation at ComapnyDto side
        // because of exception:
        /*
        com.fasterxml.jackson.databind.exc.InvalidDefinitionException:
        Cannot construct instance of `ee.middleware.demo.springboot.company.dto.CompanyDto` (no Creators, like default construct, exist):
        cannot deserialize from Object value (no delegate- or property-based Creator)
        at [Source: (String)"{"id":1,"name":"Test-Company","description":"For testing purposes only"}"; line: 1, column: 2]
        */
        CompanyDto responsedCompany = new ObjectMapper().readValue(response.getContentAsString(), CompanyDto.class);


        // then
        verify(companyService, times(1)).findByName(company.getName());

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(responsedCompany.getName()).isEqualTo(company.getName());
    }

    @Test
    public void doNotFindCompanyByName() throws Exception {
        // given
        Company company = getNewCompany();

        // when
        Mockito.when(companyService.findByName(company.getName()))
                .thenThrow(new ResourceNotFoundException("Company not found by name: " + company.getName()));

        // do
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/company?name=" + company.getName())
                .accept(MediaType.APPLICATION_JSON);

        MockHttpServletResponse response = mockMvc
                .perform(requestBuilder)
                .andReturn()
                .getResponse();

        // then
        verify(companyService, times(1)).findByName(company.getName());
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    private Company getNewCompany() {
        return Company.builder()
                .id(1L)
                .name("Test-Company")
                .description("For testing purposes only")
                .createdAt(new Date())
                .createdBy("test")
                .build();
    }

}
