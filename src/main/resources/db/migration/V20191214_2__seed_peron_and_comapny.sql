INSERT INTO person (name, description, created_by)
VALUES  ('John Smith', 'Universal worker', 'InitialScript'),
        ('Artur Kass', 'Animal lover', 'InitialScript'),
        ('Sebastian Loeb', 'Driver', 'InitialScript'),
        ('Peeter Tamm', 'The First One - Senior', 'InitialScript'),
        ('Peeter Tamm', 'The Second One - Junior', 'InitialScript'),
        ('Margus Nairis', 'Developer', 'InitialScript');

INSERT INTO company(name, description, created_by)
VALUES  ('Coca-Cola', 'Drinks', 'InitialScript'),
        ('Middleware OÜ', 'Integrations and all other solutions', 'InitialScript'),
        ('Father & Son Co', 'Family business', 'InitialScript');

INSERT INTO person_company(person_id, company_id)
VALUES  (1,1),
        (1,2),
        (1,3),
        (4,3),
        (5,3),
        (6,2);