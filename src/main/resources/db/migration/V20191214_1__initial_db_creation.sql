-- Person. Can co-operate with many companies.
-- NB! Not unique name because of we can have couple of persons named as Peeter Tamm for example..
CREATE TABLE person(
        id                  bigserial   NOT NULL,
        name                varchar(64) NOT NULL,
        description         text,

        created_at          timestamptz     NOT NULL DEFAULT now(),
        created_by          varchar(64)     NOT NULL,
        updated_at          timestamptz,
        updated_by          varchar(64),

        CONSTRAINT person_pk PRIMARY KEY (id)
);

-- Company. Company name must be unique.
CREATE TABLE company(
        id                  bigserial   NOT NULL,
        name                varchar(64) NOT NULL,
        description         text,

        created_at          timestamptz     NOT NULL DEFAULT now(),
        created_by          varchar(64)     NOT NULL,
        updated_at          timestamptz,
        updated_by          varchar(64),

        CONSTRAINT company_pk PRIMARY KEY (id),
        CONSTRAINT company_name_unique UNIQUE (name)
);

-- Person and Company relations. Person can work with many companies.
-- Person can't work more than once for the same company
CREATE TABLE person_company(
        id            bigserial NOT NULL,
        person_id     bigserial   NOT NULL,
        company_id    bigserial NOT NULL,

        CONSTRAINT person_company_pk PRIMARY KEY (id),

        CONSTRAINT fk_person_id
            FOREIGN KEY(person_id)
            REFERENCES person(id),

        CONSTRAINT fk_company_id
            FOREIGN KEY(company_id)
            REFERENCES company(id),

        CONSTRAINT unique_person_company UNIQUE (person_id, company_id)
);


COMMENT ON TABLE person
    IS 'Provides information about of person. NB! We can have multiple persons with exactly the same name!';

COMMENT ON TABLE company
        IS 'Provides information about of company. NB! Comapny name must be unique!';


COMMENT ON TABLE person_company
                IS 'Provides information about of person and company/companies realation(s)';

