package ee.middleware.demo.springboot.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class User {

    // TODO: fill from auth provider JWT token
    private String username;
    // email
    // etc..

}
