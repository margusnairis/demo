package ee.middleware.demo.springboot.company.service.impl;

import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.company.form.CompanyForm;
import ee.middleware.demo.springboot.company.repository.CompanyRepository;
import ee.middleware.demo.springboot.company.service.CompanyService;
import ee.middleware.demo.springboot.exception.ResourceNotFoundException;
import ee.middleware.demo.springboot.person_company.PersonCompany;
import ee.middleware.demo.springboot.person_company.PersonCompanyRepository;
import ee.middleware.demo.springboot.user.User;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Log4j2
@Service
//@Service("mainCompanyService") -- use when many services implements the same interface
@AllArgsConstructor
public class MainCompanyService implements CompanyService {

    private final CompanyRepository companyRepository;
    private final PersonCompanyRepository personCompanyRepository;
    private final User user;

    @Override
    public List<Company> findAll() {

        return new ArrayList<>(companyRepository.findAll());
    }

    @Override
    public Company findById(Long id) {
        return companyRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Company not found by id: " + id));
    }

    @Override
    public Company findByName(String name) {
        return companyRepository.findByName(name)
                .orElseThrow(() -> new ResourceNotFoundException("Company not found by name: " + name));
    }

    @Override
    public Company create(CompanyForm form) {
        Company company = Company.builder()
                .name(form.getName())
                .description(form.getDescription())
                .createdAt(new Date())
                .createdBy(user.getUsername())
                .build();
        // The save() method returns the saved entity, including the updated id field.
        // We return dto instead of entity itself because of keeping integrity of data in DB.
        return companyRepository.save(company);
    }

    @Override
    public Company update(Long id, CompanyForm form) {
        Company company = findById(id).toBuilder()
                .name(form.getName())
                .description(form.getDescription())
                .updatedAt(new Date())
                .updatedBy(user.getUsername())
                .build();

        return companyRepository.save(company);
    }

    @Override
    public void delete(Long id) {
        Company company = findById(id);
        // Remove company-person relation(s) first
        List<PersonCompany> personCompanies = personCompanyRepository.findByCompany(company);
        for (PersonCompany personCompany: personCompanies) {
            personCompanyRepository.delete(personCompany);
        }
        // Delete company
        companyRepository.delete(company);
        log.info("Company '{}' deleted by '{}'", company.getName(), user.getUsername());
    }

}
