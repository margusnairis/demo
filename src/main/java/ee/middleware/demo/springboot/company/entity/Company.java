package ee.middleware.demo.springboot.company.entity;

import ee.middleware.demo.springboot.company.dto.CompanyDto;
import ee.middleware.demo.springboot.person.Person;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "company")
public class Company {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = 64)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "companies",
                fetch = FetchType.LAZY)
    @Builder.Default
    private Set<Person> persons = new HashSet<>();

    // email
    // phone
    // address(es)
    // registration number
    // etc..


    // Audit changes
    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(name = "updated_by")
    private String updatedBy;


    public CompanyDto createDto () {
        return CompanyDto.builder()
                .id(id)
                .name(name)
                .description(description)
                .build();
    }
}
