package ee.middleware.demo.springboot.company.service;

import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.company.form.CompanyForm;

import java.util.List;

public interface CompanyService {

    List<Company> findAll();
    Company findById(Long id);
    Company findByName(String name);
    Company create(CompanyForm form);
    Company update(Long id, CompanyForm form);
    void delete(Long id);
}
