package ee.middleware.demo.springboot.company;

import ee.middleware.demo.springboot.company.dto.CompanyDto;
import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.company.form.CompanyForm;
import ee.middleware.demo.springboot.company.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/company", produces = "application/json")
@AllArgsConstructor
public class CompanyController {

    // @Qualifier("mainCompanyService")  -- use when many services implements the same interface
    private final CompanyService companyService;

    @GetMapping(value = "/all")
    public List<CompanyDto> findAll() {
        return companyService.findAll()
                .stream()
                .map(Company::createDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}")
    public CompanyDto findById(@PathVariable final Long id) {
        return companyService.findById(id).createDto();
    }

    @GetMapping
    public CompanyDto findByName(@RequestParam String name) {
        return companyService.findByName(name).createDto();
    }

    @PostMapping
    public CompanyDto create(@Valid @RequestBody final CompanyForm form) {
        return companyService.create(form).createDto();
    }

    @PutMapping(value = "/{id}")
    public CompanyDto update(@PathVariable Long id,
                             @Valid @RequestBody final CompanyForm form) {
        return companyService.update(id, form).createDto();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable final Long id) {
        companyService.delete(id);
        return ResponseEntity.ok().build();
    }

}
