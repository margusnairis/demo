package ee.middleware.demo.springboot.company.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class CompanyForm {

    @NotNull
    @Size(min = 3, max = 64, message = "Size is limited to 3 - 64 characters")
    private String name;
    private String description;
}
