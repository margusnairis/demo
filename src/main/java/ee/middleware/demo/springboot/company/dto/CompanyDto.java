package ee.middleware.demo.springboot.company.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class CompanyDto {

    private Long id;
    private String name;
    private String description;
}
