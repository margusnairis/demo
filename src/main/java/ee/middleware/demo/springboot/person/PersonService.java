package ee.middleware.demo.springboot.person;

import ee.middleware.demo.springboot.exception.ResourceNotFoundException;
import ee.middleware.demo.springboot.person_company.PersonCompany;
import ee.middleware.demo.springboot.person_company.PersonCompanyRepository;
import ee.middleware.demo.springboot.user.User;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Log4j2
@Service
@AllArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;
    private final PersonCompanyRepository personCompanyRepository;
    private final User user;

    // get all
    public List<Person> findAll() {

        return new ArrayList<>(personRepository.findAll());
    }

    // get by id
    public Person findById(Long id) {
        return personRepository.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Person not found by id: " + id));
    }

    // get by name
    // NB! we can have couple of persons named as Peeter Tamm for example..
    public List<Person> findByName(String name) {
        return personRepository.findByName(name);
    }

    // create
    public Person create(PersonForm form) {
        Person person = Person.builder()
                .name(form.getName())
                .description(form.getDescription())
                .createdAt(new Date())
                .createdBy(user.getUsername())
                .build();
        // The save() method returns the saved entity, including the updated id field.
        // We return dto instead of entity itself because of keeping integrity of data in DB.
        return personRepository.save(person);
    }

    // update
    public Person update(Long id, PersonForm form) {
        Person person = findById(id).toBuilder()
                .name(form.getName())
                .description(form.getDescription())
                .updatedAt(new Date())
                .updatedBy(user.getUsername())
                .build();

        return personRepository.save(person);
    }

    // delete
    public void delete(Long id) {
        Person person = findById(id);
        // Remove person-company relation(s) first
        List<PersonCompany> personCompanies = personCompanyRepository.findByPerson(person);
        for (PersonCompany personCompany: personCompanies) {
            personCompanyRepository.delete(personCompany);
        }
        // Delete person
        personRepository.delete(person);
        log.info("Person '{}' deleted by '{}'", person.getName(), user.getUsername());
    }

}
