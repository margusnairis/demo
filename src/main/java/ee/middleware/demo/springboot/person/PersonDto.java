package ee.middleware.demo.springboot.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class PersonDto {

    private Long id;
    private String name;
    private String description;
}
