package ee.middleware.demo.springboot.person;

import ee.middleware.demo.springboot.company.entity.Company;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person")
public class Person {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // NB! Not unique because of we can have couple of persons named as Peeter Tamm for example..
    @Column(name = "name", nullable = false, length = 64)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "person_company",
            joinColumns = {
                    @JoinColumn(name = "person_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "company_id")
            }
    )
    @Builder.Default
    Set<Company> companies = new HashSet<>();

    // email
    // phone
    // address(es)
    // doc number
    // personalIdCode
    // etc..

    // Audit changes
    @Column(name = "created_at", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(name = "updated_by")
    private String updatedBy;


    public PersonDto createDto () {
        return PersonDto.builder()
                .id(id)
                .name(name)
                .description(description)
                .build();
    }
}
