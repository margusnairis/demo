package ee.middleware.demo.springboot.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class PersonForm {

    @NotNull
    @Size(min = 3, max = 64, message = "Size is limited to 3 - 64 characters")
    private String name;
    private String description;
    private List<Long> companyIds;
}
