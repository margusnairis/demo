package ee.middleware.demo.springboot.person;

import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.person_company.PersonCompanyDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/person", produces = "application/json")
@AllArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping(value = "/all")
    public List<PersonDto> findAll() {
        return personService.findAll()
                .stream()
                .map(Person::createDto)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}")
    public PersonCompanyDto findById(@PathVariable final Long id) {
        Person person = personService.findById(id);
        return new PersonCompanyDto(person.createDto(),
                person.getCompanies().stream().map(Company::createDto).collect(Collectors.toList()));
    }

    @GetMapping
    public List<PersonDto> findByName(@RequestParam String name) {
        return personService.findByName(name)
                .stream()
                .map(Person::createDto)
                .collect(Collectors.toList());
    }

    @PostMapping
    public PersonDto create(@Valid @RequestBody final PersonForm form) {
        return personService.create(form).createDto();
    }

    @PutMapping(value = "/{id}")
    public PersonDto update(@PathVariable Long id,
                            @Valid @RequestBody final PersonForm form) {
        return personService.update(id, form).createDto();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable final Long id) {
        personService.delete(id);
        return ResponseEntity.ok().build();
    }

}
