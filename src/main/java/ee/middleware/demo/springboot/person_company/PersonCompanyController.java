package ee.middleware.demo.springboot.person_company;

import ee.middleware.demo.springboot.company.dto.CompanyDto;
import ee.middleware.demo.springboot.person.PersonDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class PersonCompanyController {

    private final PersonCompanyService personCompanyService;


    // Endpoints: Company <-> Person

    @GetMapping(value = "company/{companyId}/persons")
    public List<PersonDto> findCompanyPersons(@PathVariable final Long companyId) {
        return personCompanyService.findCompanyPersons(companyId);
    }

    @PutMapping(value = "company/{companyId}/persons/{personId}")
    public CompanyPersonDto addCompanyPerson(@PathVariable final Long companyId,
                                            @PathVariable final Long personId) {

        return personCompanyService.addCompanyPerson(companyId, personId);
    }


    // endpoints: Person <-> Company

    @GetMapping(value = "person/{personId}/companies")
    public List<CompanyDto> findPersonCompanies(@PathVariable final Long personId) {
        return personCompanyService.findPersonCompanies(personId);
    }

    @PutMapping("/person/{personId}/companies/{companyId}")
    public PersonCompanyDto addPersonCompany(@PathVariable final Long personId,
                                             @PathVariable final Long companyId) {
        return personCompanyService.addPersonCompany(personId, companyId);
    }
}
