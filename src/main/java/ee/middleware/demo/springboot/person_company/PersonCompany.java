package ee.middleware.demo.springboot.person_company;

import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.person.Person;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person_company")
public class PersonCompany {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "company_id")
    Company company;

    @ManyToOne
    @JoinColumn(name = "person_id")
    Person person;
}
