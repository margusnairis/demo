package ee.middleware.demo.springboot.person_company;

import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.company.dto.CompanyDto;
import ee.middleware.demo.springboot.company.repository.CompanyRepository;
import ee.middleware.demo.springboot.company.service.CompanyService;
import ee.middleware.demo.springboot.person.Person;
import ee.middleware.demo.springboot.person.PersonDto;
import ee.middleware.demo.springboot.person.PersonRepository;
import ee.middleware.demo.springboot.person.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PersonCompanyService {

    private final CompanyService companyService;
    private final PersonService personService;

    private final CompanyRepository companyRepository;
    private final PersonRepository personRepository;


    // Methods: Company <-> Person

    public List<PersonDto> findCompanyPersons(Long companyId) {
        return getCompanyPersonDtos(companyService.findById(companyId));
    }

    @Transactional(rollbackFor = {RuntimeException.class})
    public CompanyPersonDto addCompanyPerson(Long companyId, Long personId) {
        Company company = companyService.findById(companyId);
        Person person = personService.findById(personId);

        company.getPersons().add(person);
        person.getCompanies().add(company);

        companyRepository.save(company);

        return new CompanyPersonDto(company.createDto(), getCompanyPersonDtos(company));
    }

    private List<PersonDto> getCompanyPersonDtos(Company company) {
        return company.getPersons()
                .stream()
                .map(Person::createDto)
                .collect(Collectors.toList());
    }


    // Methods: Person <-> Company

    public List<CompanyDto> findPersonCompanies(Long personId) {
        return getPersonCompanyDtos(personService.findById(personId));
    }

    @Transactional(rollbackFor = {RuntimeException.class})
    public PersonCompanyDto addPersonCompany(Long personId, Long companyId) {
        Person person = personService.findById(personId);
        Company company = companyService.findById(companyId);

        person.getCompanies().add(company);
        company.getPersons().add(person);

        personRepository.save(person);

        return new PersonCompanyDto(person.createDto(), getPersonCompanyDtos(person));
    }

    private List<CompanyDto> getPersonCompanyDtos(Person person) {
        return person.getCompanies()
                .stream()
                .map(Company::createDto)
                .collect(Collectors.toList());
    }

}
