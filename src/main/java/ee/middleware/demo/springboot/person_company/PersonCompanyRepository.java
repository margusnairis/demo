package ee.middleware.demo.springboot.person_company;

import ee.middleware.demo.springboot.company.entity.Company;
import ee.middleware.demo.springboot.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonCompanyRepository extends JpaRepository<PersonCompany, Long> {
    List<PersonCompany> findByCompany(Company company);
    List<PersonCompany> findByPerson(Person person);
}
