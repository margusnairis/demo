package ee.middleware.demo.springboot.person_company;

import ee.middleware.demo.springboot.company.dto.CompanyDto;
import ee.middleware.demo.springboot.person.PersonDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class PersonCompanyDto {

    private PersonDto person;
    private List<CompanyDto> companies;
}
