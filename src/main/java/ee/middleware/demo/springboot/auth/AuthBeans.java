package ee.middleware.demo.springboot.auth;

import ee.middleware.demo.springboot.user.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
public class AuthBeans {

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST,
            proxyMode = ScopedProxyMode.TARGET_CLASS)
    public User getUser() {

/*        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder
                        .currentRequestAttributes()).getRequest();
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) request.getUserPrincipal();
        KeycloakPrincipal<?> principal=(KeycloakPrincipal<?>)token.getPrincipal();
        KeycloakSecurityContext session = principal.getKeycloakSecurityContext();*/

        return new User(
                "SystemUser"
/*                principal.getName(),
                session.getToken().getPreferredUsername(),
                session.getToken().getEmail()*/
        );

    }
}
