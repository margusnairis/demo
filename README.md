# SpringBoot REST API demo
Project SDK: Java 11
## How to start:
Prepare and configure your datasource.  
By default in <i>resources/application.properties</i> is set:  
<b>spring.datasource.url = jdbc:postgresql://localhost:5432/home-rest</b>  

Database setup files for flyway:  
src/main/resources/db/migration/*.sql

### Run the project: 
./gradlew bootRun

### Endpoints documentation by Swagger2
When project running the additional documentation aobut of controllers and enpoints is published by Swagger2  
http://localhost:8088/swagger-ui.html#/

### Postman collection to make sample API calls
src/main/resources/postman/Home-REST.postman_collection.json

### Used dependencies:
* spring-boot-starter-web
* spring-boot-starter-data-jpa
* spring-boot-starter-test


* flyway-core 
* postgresql:42.2.2


* springfox-swagger2:2.9.2
* springfox-swagger-ui:2.9.2


* lombok

* spring-boot-starter-actuator


